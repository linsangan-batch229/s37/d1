const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js');


//check email if it existing to our database
router.post("/checkEmail", (req, res) => {
    userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//user registration route
router.post("/register", (req, res) => {
    userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


//User Login
router.post("/login", (req, res) => {
    userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// para maging global si router
//available sa ibang files
module.exports = router;